const app = require('express')();
const server = require('http').Server(app);
const socketio = require('socket.io')(server);

require('./app_back/routes/routes')(app);

//let time = setInterval(showTime, 1000);

server.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});

function showTime() {
  var date = new Date();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  console.log(hours + ':' + minutes + ':' + seconds);
}

module.exports = socketio;
