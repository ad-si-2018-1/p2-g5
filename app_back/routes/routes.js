// módulo usado para lidar com caminhos de arquivos
let api = require('../api/api');
let express = require('express');
let bodyParser = require('body-parser'); // processa corpo de requests
let cookieParser = require('cookie-parser'); // processa cookies

module.exports = app => {
  app.use(express.static('app_front'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());

  // lidando com GETS
  app.route('/').get(api.home);
  app.route('/:mode/:args').get(api.resultCommand);

  // lidando com POSTS
  app.route('/gravar_mensagem').post(api.gravarMensagem);
  app.route('/login').post(api.login);
  app.route('/result_command').post(api.resultCommand);
};
